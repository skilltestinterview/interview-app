import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PExm1aComponent } from './components/p-exm1a/p-exm1a.component';

const routes: Routes = [
  { path: '', redirectTo: 'example', pathMatch: 'full' },
  { path: 'example', component: PExm1aComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
