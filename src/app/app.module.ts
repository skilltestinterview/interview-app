import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PExm1aComponent } from './components/p-exm1a/p-exm1a.component';
import { FormsModule } from '@angular/forms';
// primeng
import { InputTextModule } from 'primeng/inputtext';
import { ProductOrderService } from './service/products-orders.service';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputNumberModule } from 'primeng/inputnumber';

@NgModule({
  declarations: [AppComponent, PExm1aComponent],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // primeng
    InputTextModule,
    ButtonModule,
    InputNumberModule,
    TableModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
