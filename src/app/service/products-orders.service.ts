import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Data } from '../model/data.model';

@Injectable({
  providedIn: 'root',
})
export class ProductOrderService {
  endpoint = 'assets/data/products-orders.json';
  constructor(private http: HttpClient) {}

  getData(): Observable<Data> {
    return this.http.get(this.endpoint) as Observable<Data>;
  }
}
