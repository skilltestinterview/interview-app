import { Component, ViewChild } from '@angular/core';
import { ProductOrderService } from '../../service/products-orders.service';
import { DataList } from '../../model/data.model';
import { Table } from 'primeng/table';
@Component({
  selector: 'app-p-exm1a',
  templateUrl: './p-exm1a.component.html',
  styleUrl: './p-exm1a.component.scss',
})
export class PExm1aComponent {
  datas!: DataList[];
  clonedDatas: { [s: string]: DataList } = {};

  constructor(private productOrderService: ProductOrderService) {}

  public ngOnInit(): void {
    this.productOrderService.getData().subscribe((x) => {
      this.datas = x.data;
    });
  }

  @ViewChild('table') table!: Table;
  onRowAdd() {
    var data = {
      id: '',
      code: '',
      name: '',
      quantity: 0,
    } as DataList;
    this.datas.push(data);

    this.clonedDatas[data.id] = { ...data };
    this.table.initRowEdit(this.table.value[this.datas.length - 1]);
  }

  onRowDelete(index: number) {
    this.datas.splice(index, 1);
  }

  onRowEditInit(data: DataList) {
    this.clonedDatas[data.id] = { ...data };
  }

  onRowEditSave(data: DataList) {
    delete this.clonedDatas[data.id];
  }

  onRowEditCancel(data: DataList, index: number) {
    this.datas[index] = this.clonedDatas[data.id];
    delete this.clonedDatas[data.id];
  }

  sumQuantity() {
    return this.datas?.reduce((acc, cur) => acc + Number(cur.quantity), 0);
  }
}
